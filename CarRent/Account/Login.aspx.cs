﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using CarRent.Models;
using System.Data;

namespace CarRent.Account
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterHyperLink.NavigateUrl = "Register";
            // Enable this once you have account confirmation enabled for password reset functionality
            //ForgotPasswordHyperLink.NavigateUrl = "Forgot";
            OpenAuthLogin.ReturnUrl = Request.QueryString["ReturnUrl"];
            var returnUrl = HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
            if (!String.IsNullOrEmpty(returnUrl))
            {
                RegisterHyperLink.NavigateUrl += "?ReturnUrl=" + returnUrl;
            }
        }

        protected void LogIn(object sender, EventArgs e)
        {
            if (IsValid)
            {
                // Validate the user password
               // var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
               // var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

                // This doen't count login failures towards account lockout
                // To enable password failures to trigger lockout, change to shouldLockout: true
                //int result =3; //;= signinManager.PasswordSignIn(Email.Text, Password.Text, RememberMe.Checked, shouldLockout: false);
                DataTable loginDT = DAL.DAL.Instance.ExecuteDT(String.Format($"Select isnull(banned,1) as banned, [User].id , Firstname, roleid from [User] inner join person on person.id = personid where [Login] = '{Email.Text}' and [Password] = '{Password.Text}'"));
                if(loginDT != null && loginDT.Rows.Count !=0)
                {
                    if ((bool)loginDT.Rows[0]["banned"])
                    {
                        Response.Redirect("/Account/Lockout");
                    }
                    else
                    {
                        //IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                        Session["UserName"] = (string)loginDT.Rows[0]["FirstName"];
                        Session["UserID"] = (int)loginDT.Rows[0]["id"];
                        Session["Role"] = (int)loginDT.Rows[0]["RoleID"];
                        Response.Redirect("..//Default.aspx");
                    }
                }
                else
                {
                    FailureText.Text = "Invalid login attempt";
                    ErrorMessage.Visible = true;
                    
                }
            }
        }
    }
}