﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using CarRent.Models;
using System.Data;

namespace CarRent.Account
{
    public partial class Register : Page
    {
        protected void CreateUser_Click(object sender, EventArgs e)
        {
            //var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
          //  var signInManager = Context.GetOwinContext().Get<ApplicationSignInManager>();
            //var user = new ApplicationUser() { UserName = Email.Text, Email = Email.Text };
            DataTable CheckUsers = DAL.DAL.Instance.ExecuteDT($"Select * from [User] where Login = '{Email.Text}'");
            if(CheckUsers.Rows.Count == 0)
            {
                DataTable Person = DAL.DAL.Instance.ExecuteDT($@"INSERT INTO [Person]
                                                                               ([FirstName]
                                                                               ,[LastName]
                                                                               
                                                                               ,[Address]
                                                                               ,[City]
                                                                               ,[Country])
		                                                                       output INSERTED.ID
                                                                         VALUES
                                                                               ('{FirstNameTB.Text}'
                                                                               ,'{LastNameTB.Text}'                                                                  
                                                                               ,'{AddressTB.Text}'
                                                                               ,'{CityTB.Text}'
                                                                               ,'{CountryTB.Text}') ");
                if(Person.Rows.Count == 1)
                {
                    DAL.DAL.Instance.ExecuteDT($@"INSERT INTO [User]
                                                       ([PersonID]
                                                       ,[Banned]
                                                       ,[RoleID]
                                                       ,[Login]
                                                       ,[Password])
                                                 VALUES
                                                       ({(int)Person.Rows[0]["ID"]}
                                                       ,0
                                                       ,2
                                                       ,'{Email.Text}'
                                                       ,'{Password.Text}')");
                    Response.Redirect("Login.aspx");
                }

            }
            else
            {
                ErrorMessage.Text = "Email is already used";
            }
           
        }
    }
}