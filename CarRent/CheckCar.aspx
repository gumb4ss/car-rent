﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CheckCar.aspx.cs" Inherits="CarRent.CheckCar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        Logged in as :<asp:Label ID="Userlbl" runat="server" Text="User"></asp:Label>
    </p>
    <p>
        Brand:
        <asp:Label ID="Brand" runat="server" Text="Brand"></asp:Label>
    </p>
    <p>
        Model:
        <asp:Label ID="Model" runat="server" Text="Model"></asp:Label>
    </p>
    <p>
        Cost:&nbsp;&nbsp;
        <asp:Label ID="Cost" runat="server" Text="Cost"></asp:Label>
    </p>
    <p>
        Car Type :
        <asp:Label ID="CarType" runat="server" Text="CarType"></asp:Label>
    </p>
    <p>
        Date Start:
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
&nbsp;( yyyy-MM-dd Format please)</p>
    <p>
        Date End:
        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
&nbsp;( yyyy-MM-dd Format please)</p>
    <p>
        <asp:Button ID="Rentbtn" runat="server" OnClick="Rentbtn_Click" Text="Rent" />
    </p>
</asp:Content>
