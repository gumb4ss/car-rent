﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarRent
{
    public partial class CheckCar : System.Web.UI.Page
    {
        DataTable CheckedCar = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
                Rentbtn.Enabled = false;
            else
                Rentbtn.Enabled = true;

            if(Session["checkedCarID"]!=null)
                LoadData( (int)Session["checkedCarID"]);
            else
                Response.Redirect("Default.aspx");
        }
        void LoadData(int id)
        {
            CheckedCar = DAL.DAL.Instance.ExecuteDT(String.Format($@"SELECT 
                                                                          [Model]
                                                                          ,[Cost]
                                                                          
                                                                          , CarType.[name]
                                                                          ,[Brand]
                                                                      FROM [Car] inner join Cartype on cartypeid = cartype.id where car.id = {id}"));
            if(CheckedCar != null)
            {
                Userlbl.Text = (string)Session["UserName"];
                Brand.Text = (string)CheckedCar.Rows[0]["Brand"];
                Model.Text = (string)CheckedCar.Rows[0]["Model"];
                Cost.Text = ((double)CheckedCar.Rows[0]["Cost"]).ToString();
                CarType.Text = (string)CheckedCar.Rows[0]["name"];

            }

        }

        protected void Rentbtn_Click(object sender, EventArgs e)
        {
            DAL.DAL.Instance.ExecuteDT(String.Format($"insert into Rent (StartDate, EndDate, Returned, UserID, CarID) Values('{TextBox1.Text}', '{TextBox2.Text}', 0, {(int)Session["UserID"]}, {(int)Session["checkedCarID"]})"));
            Session["checkedCarID"] = null;
            Response.Redirect("Default.aspx");
        }
    }
}