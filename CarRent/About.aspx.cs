﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarRent
{
    public partial class About : Page
    {
        DataTable RentedCars = new DataTable();
        DataTable cars = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            cars = DAL.DAL.Instance.ExecuteDT("Select Brand, Model, Cost, car.id from Car left outer join rent on carID = car.id and returned = 0 where rent.id is null");
            try
            {
                if ((int)Session["Role"] == 1)
                {
                    RentedCars = DAL.DAL.Instance.ExecuteDT($@"Select rent.id, Brand, Model, Cost, FirstName, LastName, StartDate, EndDate
                                                               from Car
                                                              left outer
                                                               join rent on carID = car.id and returned = 0
                                                              inner join[user] on[user].id = userid
                                                              inner join person on personid = person.id where rent.id is not null");
                    GridView1.DataSource = RentedCars;
                    GridView1.DataBind();
                    GridView1.RowCommand += GridView1_RowCommand;
                }
                else
                {
                    RentedCarsLbl.Visible = false;
                    GridView1.Visible = false;
                }
            }
            catch
            {
                RentedCarsLbl.Visible = false;
                GridView1.Visible = false;
            }
            CarsGV.DataSource = cars;
            CarsGV.DataBind();
            CarsGV.RowCommand += CarsGV_RowCommand;
        }

        private void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = 0;

            GridView grid = sender as GridView;
            if (e.CommandName == "return")
            {
                index = Convert.ToInt32(e.CommandArgument);
                DAL.DAL.Instance.ExecuteDT(String.Format($"update rent set Returned = 1 where id ={(int)RentedCars.Rows[index]["id"]}"));
                Response.Redirect("About.aspx");
            }
        }

        private void CarsGV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = 0;
            
            GridView grid = sender as GridView;
            if (e.CommandName == "check")
            {
                index = Convert.ToInt32(e.CommandArgument);
                Session["checkedCarID"] = (int)cars.Rows[index]["id"];
                Response.Redirect("CheckCar.aspx");
            }
        }

        protected void CarsGV_SelectedIndexChanged(object sender, EventArgs e)
        {
            

        }
    }
}