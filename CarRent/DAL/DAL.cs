﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CarRent.DAL
{
    public sealed class DAL
    {
        private object locker = new object();

        #region singleton
        private static readonly DAL instance = new DAL();

        private DAL()
        { }

        public static DAL Instance
        {
            get
            {
                return instance;
            }
        }
        #endregion

        private SqlConnection conn;

        public static string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            }
        }

        public int ExecuteNonQuery(string query, params SqlParameter[] parameters)
        {
            lock (locker)
            {
                if (conn == null)
                {
                    conn = new SqlConnection(ConnectionString);
                }
                try
                {
                    using (var command = new SqlCommand(query, conn))
                    {
                        command.Parameters.AddRange(parameters);
                        command.CommandTimeout = 240;
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Open();
                        }
                        return command.ExecuteNonQuery();
                    }
                }
                catch (SqlException ex)
                {
                    return -1;
                }
            }
        }

        public object ExecuteScalar(string query, params SqlParameter[] parameters)
        {
            lock (locker)
            {
                if (conn == null)
                {
                    conn = new SqlConnection(ConnectionString);
                }
                try
                {
                    using (var command = new SqlCommand(query, conn))
                    {
                        command.Parameters.AddRange(parameters);
                        command.CommandTimeout = 240;
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Open();
                        }
                        return command.ExecuteScalar();
                    }
                }
                catch (SqlException ex)
                {
                    return null;
                }
            }
        }
        public DataTable ExecuteDT(string name)
        {
            if (conn == null)
            {
                conn = new SqlConnection(ConnectionString);
            }
            try
            {

                using (SqlCommand command = new SqlCommand(name, conn))
                {
                    using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                    {
                        DataTable target = new DataTable();
                        if (conn.State != ConnectionState.Open)
                            conn.Open();
                        adapter.Fill(target);
                        return target;
                    }
                }

            }
            catch (Exception e)
            {
                return new DataTable();
            }
        }
        public SqlDataReader ExecuteReader(string query, params SqlParameter[] parameters)
        {
            lock (locker)
            {
                if (conn == null)
                {
                    conn = new SqlConnection(ConnectionString);
                }
                try
                {
                    using (var command = new SqlCommand(query, conn))
                    {
                        command.Parameters.AddRange(parameters);
                        command.CommandTimeout = 240;
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Open();
                        }
                        return command.ExecuteReader();
                    }
                }
                catch (SqlException ex)
                {
                    return null;
                }
            }
        }
    }
}